timer_start = Time.now

st = 1
nd = 1
num = 0
array = []

while num <= 4000000
    num = st + nd
    st = nd
    nd = num
    puts num
    if num % 2 == 0
      puts "Adding to array #{num}"
      array.push(num)
    end
end

puts "#{array}"
puts "Even numbers sum: #{array.inject(:+)}"
puts "Elapsed Time: #{(Time.now - timer_start)*1000} milliseconds"