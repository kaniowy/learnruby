array = []

(1...1000).each do |n|
  if n % 3 == 0 || n % 5 == 0
    array.push(n)
  end
end

puts array.inject(:+)