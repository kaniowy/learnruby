puts "Input number"
num = $stdin.gets.chomp.to_i

def tester(n)
  if n <  100
    puts "Under 100"
  elsif n < 1000 && n > 100
    puts  "100 < n < 1000 "
  elsif n < 2000 && n > 1000
    puts  "1000 < n < 2000 "
  end
end

tester(num)