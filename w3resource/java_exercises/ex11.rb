puts "Input radius"
num = $stdin.gets.chomp.to_i

def circle(a)
  perimeter = 2 * 3.14 * a
  area = 3.14 * (a * a)
  puts "Perimeter: #{perimeter}; Area: #{area}"
end

circle(num)