
puts "Podaj dystans"
distance = $stdin.gets.chomp.to_f
puts "Podaj czas: h-m-s"
time = $stdin.gets.chomp.to_s

time_table = time.split("-").map(&:to_f)

def calc(a,b)
  seconds = ( b[0] * 3600) + (b[1] * 60) + b[2]
  mile = a / 1609
  met_sec = a / seconds
  km_h = (a/1000) / (seconds/3600)
  mil_h = mile / (seconds/3600)
  
  puts "Your speed in meters/second is #{met_sec}"
  puts "Your speed in km/h is #{km_h}"
  puts "Your speed in miles/h is #{mil_h}"
end

calc(distance,time_table)
