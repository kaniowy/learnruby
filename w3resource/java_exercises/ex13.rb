puts "Input width"
width = $stdin.gets.chomp.to_f

puts "Input height"
height = $stdin.gets.chomp.to_f


def rectangle(a,b)
  area = a * b
  perimeter = 2 * (a + b)
  puts "Perimeter: #{perimeter}; Area: #{area}"
end

rectangle(width,height)