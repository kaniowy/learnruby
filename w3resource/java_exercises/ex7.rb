puts "Input number"
first_num = $stdin.gets.chomp.to_i

def multiplication(a)
  b = 1
  until b == a
    puts a * b
    b += 1
  end
end

multiplication(first_num)