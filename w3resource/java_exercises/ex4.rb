def zad_a(a,b,c)
  result = a + b * c
  puts result
end

def zad_b(a,b,c)
  result = (a + b ) % 9
  puts result
end

def zad_c(a,b,c,d)
  result = a + b * c / d
  puts result
end

def zad_d(a,b,c,d,e,f)
  result = a + b / c * d - e % f
  puts result
end

zad_a(-5,8,6)
zad_b(55,9,9)
zad_c(20,-3,5,8)
zad_d(5,15,3,2,8,3)


