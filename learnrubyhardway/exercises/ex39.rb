states = {
        'Oregon' => 'OR',
        'Florida' => 'FL',
        'California' => 'CA',
        'New York' => 'NY',
        'Michigan' => 'MI'
    }

cities = {
        'CA' => 'San Francisco',
        'MI' => 'Detroit',
        'FL' => 'Jacksonville'
    }

puts states
puts cities

cities['NY'] = 'New York'
cities['OR'] = 'Portland'

puts '-' * 10
puts states
puts cities

puts '-' * 10
puts "NY state has: #{cities['NY']}"
puts "OR state has: #{cities['OR']}"

puts '-' * 10
puts "Michigan's abbreviation is: #{states['Michigan']}"
puts "Florida's abbreviation is: #{states['Florida']}"

puts '-' * 10
puts "Michigan has: #{cities[states['Michigan']]}"
puts "Florida has: #{cities[states['Florida']]}"

puts '-' * 10
states.each do |state, abbrev|
  puts "#{state} is abrriviated #{abbrev}"
end

puts '-' * 10
cities.each do |abrrev, city|
  puts "#{abrrev} has the city #{city} "
end

puts '-' * 10
states.each do |state,abbrev|
  city = cities[abbrev]
  puts "State: #{state}, Abbreaviation: #{abbrev}, City: #{city}"
end

puts '-' * 10
state = states=['Texas']

if !state
  puts "Sorry, no for texas."
end

  city = cities['TX']
city ||= "Does Not Exist"

puts "The city for the state TX is: #{city}"