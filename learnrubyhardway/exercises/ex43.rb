class Map
  def initialize(start_scene)
  end

  def next_scene(scene_name)
  end

  def opening_scene
  end
end

class Engine
  def initialize(scene_map)
  end

  def play()
  end
end

class Scene
  def enter()
    puts "This scene is not yet configured. Subclass it and implement enter()."
    exit(1)
  end
end

class Death < Scene
  def enter()

  end
end

class Central_corridor < Scene
  def enter()

  end
end

class Laser_weapon_armory < Scene
  def enter()

  end
end

class The_bridge < Scene
  def enter()

  end
end

class Escape_pod < Scene
  def enter()

  end
end

a_map = Map.new('central_corridor')
a_game = Engine.new(a_map)
a_game.play()
