ten_things = "Apples Oranges Crows Telephone Light Sugar"

puts "Wait there are not 10 thing in that list. Let's fix that."

stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]
more_stuff = more_stuff.shuffle

while stuff.length < 10
  new_stuff = more_stuff.pop
  puts "Adding #{new_stuff}"
  stuff.push(new_stuff)
  puts "There are #{stuff.length} items now"
end

puts "There we go: #{stuff}."

puts stuff[1]
puts stuff[-1] #last
puts stuff.pop
puts stuff.join(',')
puts stuff[3...5].join('#')
