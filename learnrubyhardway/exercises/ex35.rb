def start
  puts "You are in a dark room."
  puts "There is a door to your left and right."
  puts "Which one do you take ?"

  print "> "

  room = $stdin.gets.chomp

  if room == "right"
    cthulu_room
  elsif room == "left"
    bear_room
  else
    dead("You stamble around the room until you starve.")
  end
end

def gold_room
  puts "This room is full of gold. How much do you take ?"
  print "> "

  choice = $stdin.gets.chomp

  if choice.include?("0") || choice.include?("1")
    how_much = choice.to_i
  else
    dead("Man learn to type a number")
  end

  if how_much < 50
    puts "Nice, you're not greedy. You win!"
    exit(0)
  else
    dead("Ypu are greedy bastard!")
  end
end

def cthulu_room
  puts "Here you see the great evil Cthulu."
  puts "He, it, whatever stares at you and you go insane"
  puts "Do you flee your life or eat your head?"

  print "> "
  choice = $stdin.gets.chomp

  if choice.include? "flee"
    start
  elsif choice.include? "head"
    dead("Well that was tasty!")
  else
    cthulu_room
  end
end

def bear_room
  puts "There is a bear here."
  puts "The bear has a bunch of honey"
  puts "The fat bear is in front od another door."
  puts "How are you going to move the bear?"
  bear_moved = false

  while true
    print "> "
    choice = $stdin.gets.chomp

    if choice == "take honey"
      dead("The bear looks at you then slaps your face off")
    elsif choice == "taunt bear" && !bear_moved
      puts "The bear moved from the door. You can go through it now."
      bear_moved = true
    elsif choice == "taunt bear" && bear_moved
      dead("The bear gets pissed off and chews yout legs off")
    elsif choice == "open door" && bear_moved
      gold_room
    else
      dead("You stamble around the room until you starve.")
    end
  end

end

def dead(why)
  puts why, "Good job!"
  exit(0)
end

start