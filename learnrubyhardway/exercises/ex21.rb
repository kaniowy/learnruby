def add(a,b)
  puts "Adding #{a} + #{b}..."
  return a + b
end

def subtract(a,b)
  puts "Subtracting #{a} - #{b}..."
  return a - b
end

def multiply(a,b)
  puts "Multiplying #{a} * #{b}..."
  return a * b
end

def divide(a,b)
  puts "Dividing #{a} / #{b}..."
  return a / b
end

puts "Let's do some math with just functions."

age = add(50,5)
height = subtract(100,20)
weight = multiply(2,9)
iq = divide(200,25)

puts "Age: #{age}, height: #{height}, weight: #{weight}, IQ: #{iq}"

puts "Here is a puzzle."

what=add(age,subtract(height,multiply(weight,divide(iq,2))))

puts "That becomes: #{what}. Can you do it by hand ?"
