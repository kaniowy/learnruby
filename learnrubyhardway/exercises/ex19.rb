def cheese_and_crackers(cheese_count, boxes_of_crackers)
  puts "You have #{cheese_count} cheeses!"
  puts "You have #{boxes_of_crackers} boxes_of_crackers"
  puts "Man that's enough for a party!"
  puts "Get blanket. \n"
end

puts "We can just give the function numbers directly:"
cheese_and_crackers(155,123)

puts "Or, we can use variables from our script:"
amount_of_cheese = 214
amount_of_boxes = 124
cheese_and_crackers(amount_of_cheese,amount_of_boxes)

puts "We can even do math inside too:"
cheese_and_crackers(10 + 20, 54 * 3)

puts "We can do combine the two, variables and math:"
cheese_and_crackers(amount_of_cheese + 50, amount_of_boxes + 100)
