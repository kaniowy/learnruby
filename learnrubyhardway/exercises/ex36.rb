def firstRoom
  puts "You wake up in dark room."
  puts "There is a door to your left and right."
  puts "Which one do you take"

  print "> "
  choice = $stdin.gets.chomp

  if  choice == "left"
    ninthRoom
  elsif choice == "right"
    secondRoom
  else
    dead("You stamble around the room until you starve.")
  end
end

def ninthRoom
  puts "Door to previous room was closed"
  puts "Room is divided by the small river."
  puts "Behind river you see next door. "
  puts "What you do?"

  print "> "
  first_choice = $stdin.gets.chomp

  if first_choice == "swim"
    dead("Stream of river was to stron. You drowned.")
  elsif first_choice == "jump"
    puts "Succes. You have done it."
    puts "You are next to door."
    puts "What you do?"

    print "> "
    second_choice = $stdin.gets.chomp

    if second_choice == "open door"
      twentythRoom
    else
      puts "Nothing happens."
    end
  else
    dead("You stamble around the room until you starve.")
  end
end

def dead(why)
  puts "You died", why
  exit(0)
end

firstRoom