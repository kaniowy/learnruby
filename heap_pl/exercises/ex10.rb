# Sprawdź czy podana jako parametr liczba jest liczbą pierwszą. (podzielna tylko przez 1 i przez samą siebie)

loop do
  puts "Chcesz sprawdzić czy wpisana liczba to liczba pierwsza ? [tak/nie]"
  print "> "
  answer = $stdin.gets.chomp
  if answer == "tak"
    puts "Wprowadź liczbę"
    print ">"

    n = $stdin.gets.chomp.to_i

    (2..n-1).each do |i|
      if n % i == 0
        puts "Nie jest pierwsza"
      else
        puts "Liczba jest pierwsza"
      end
    end
  elsif answer == "nie"
    puts "Żegnamy"
    break
  else
    puts "Nie ma takiej odpowiedzi"
  end
end

