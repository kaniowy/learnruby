array = []

(1..1000).each do |num|
  if num % 2 == 0 && num % 3 == 0 && num % 5 == 0 && num % 7 == 0
    array.push(num)
  end
end

puts "Biggest #{array.max}"