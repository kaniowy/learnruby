array = []
(1..1000).each do |number|
  if number % 2 != 0 && number % 3 != 0 && number % 5 != 0 && number % 7 != 0
    # puts "Adding #{number} to array"
    array.push(number)
  end
end
puts "Biggest number: #{array.max}"
