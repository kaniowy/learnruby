obj = Object.new()

def obj.one_arg(*x)
  puts "Wymagam tylko jednego argumentu #{x}"
end

obj.one_arg(4,2,31)

puts "-" * 10

def obj.two_or_more(a,b,*c)
  puts a,b,c
  puts"-" * 5
  p a,b,c
  puts"-" * 5
  puts "a: #{a}, b: #{b}, c: #{c}"
end

obj.two_or_more(1,2,3,4,5,6,7,3)

def obj.set_value(a,b,c=3)
  puts a,b,c
end

obj.set_value(1,2)