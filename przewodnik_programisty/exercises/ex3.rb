ticket = Object.new

def ticket.date
  "01/02/2016"
end

def ticket.venue
  "Sala miejska"
end

def ticket.event
  "Wieczór literacki"
end

def ticket.performance
  "Mark Twain"
end

def ticket.seat
  "Balkon drugi, rząd J. Miejsce 12"
end

def ticket.price
  5.50
end

def ticket.available?
  false
end

puts "Spektakl."
puts "Data: " + ticket.date
puts "Miejsce: " + ticket.venue
puts "Wydarzenie: " + ticket.event
puts "Wykonawca: " + ticket.performance
puts "Miejsce: " + ticket.seat
puts "Cena biletu: " + "%.2f" %ticket.price

puts "-" * 10
puts "Spektakl. Data: #{ticket.date}. Miejsce: #{ticket.venue}. Wydarzenie: #{ticket.event}. Wykonawca: #{ticket.performance}.
Miejsce: #{ticket.seat} Cena biletu: #{ticket.price}"

puts "-" * 10
if ticket.available?
  puts "Masz szczęscię"
else
  puts "Niestety to miejsce zostało sprzedane."
end