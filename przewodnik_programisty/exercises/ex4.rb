obj = Object.new

puts "Identyfikator obiektu: #{obj.object_id}"

str = "Łańcuchy również są obiektami, a to jest łańcuch!"

puts "Identyfikator łańcucha: #{str.object_id}"
puts "Identyfikator łańcucha dla 100: #{100.object_id}"

puts "-" *10
string_1 = "Witaj"
string_2 = "Witaj"
puts "Identyfikator obiektu string_1 to #{string_1.object_id}."
puts "Identyfikator obiektu string_2 to #{string_2.object_id}."